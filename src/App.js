import React, { Component } from 'react';
import Table from './Table'
import './App.css';

class App extends Component {
  // in this component class constructor we set the default state of the component and bind onClick function (this could be done in line, of course, but binding it in the constructor improves performance)
  constructor() {
    super()
    this.state = {
      "offset": 10,
      "perPage": 10,
      "items": [],
      "previous": false,
      "next": true
    }

    this.updateOffset = this.updateOffset.bind(this)
  }


  // This one is called whenever we need to query the API/DB
  queryDB(offset, perPage) {
    // here we fetch the data from the API
    const url = `https://exam.net-inspect.com/qpl?offset=${offset}&pageSize=${perPage}`

    // TODO: catch errors, and render them
    fetch(url,
      {
        method: "GET",
        cache: "no-cache",
        // here we'd include cors
        headers: {
          "Content-Type": "application/json",
          // Authorization header lives in untracked .env as REACT_APP_AUTHORIZATION_HEADER=...
          "Authorization": `Key ${process.env.REACT_APP_AUTHORIZATION_HEADER}`
        },
      }
    )
      .then(res => {
        return res.json();
      })
      .then(json => {
        // when we get an empty response, and we not on the first page at that moment, it likely means we ran out of entries in the DB. We disable Next button (we'd also display a nice "no entries found message here as well")
        if (json.length === 0 && this.state.previous) {
          this.setState({
            "next": false
          })
        }
        this.setState({
          "items": json
        });
      });
  }

  // When a user clicks on one of the pagination buttons, we update offset, and query the API for new offset data. We'd handle "How many entries on the page do you want to see in a similar fashion)
  updateOffset(e) {
    e.preventDefault()
    switch (e.target.innerText) {
      case "Previous":
        let newOffset = this.state.offset - 10;
        if (newOffset <= 10) {
          this.setState({
            previous: false,
            offset: 10
          })
        } else {
          this.setState({
            previous: true,
            next: true,
            offset: newOffset
          })
        }
        // due to how React works, below we can't use state (where our new offset should be stored by now), and we're using newOffset directly
        this.queryDB(newOffset, this.state.perPage)
        break;
      case "Next":
        newOffset = this.state.offset + 10;
        this.setState({
          previous: true,
          offset: newOffset
        })
        this.queryDB(newOffset, this.state.perPage)
        break;
      default:
        // If this ever happens, it means that the application is broken and someone needs to look into it.
        this.setState({
          previous: false,
          offset: 10
        })
        this.queryDB(10, this.state.perPage)
        console.error(`There's an error with pagination buttons. ${e.target.innerText} is neither Previous nor Next.`);
    }
  }


  // whenever the page is loaded (or the application component gets mounted in some other way), we request data from the API
  componentDidMount() {
    this.queryDB(this.state.offset, this.state.perPage)
  }

  render() {
    return (
      <div className="App">
        <h1>View QPL List</h1>
        {/* TODO: reafactor as a component for reusability and cleaner code */}
        <p><button onClick={this.updateOffset} disabled={this.state.offset <= 10 ? true : false}>Previous</button> <span>{this.state.offset / 10} page</span> <button onClick={this.updateOffset} disabled={this.state.next ? false : true}>Next</button></p>
        <div>
          <Table items={this.state.items} />
        </div>
      </div>
    );
  }
}

export default App;
