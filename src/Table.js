import React, { Component } from 'react';
import Element from './Element'
import './App.css';

class Table extends Component {
  render() {
    return (
      <table>
        <thead>
          <tr className="tableHeader">
            <th>Part Number</th>
            <th>Part Revision</th>
            <th>Part Name</th>
            <th>Tool / Die Set Number</th>
            <th className="boolean">QPL</th>
            <th className="boolean">Open PO</th>
            <th>Part Jurisdiction</th>
            <th>Part Classification</th>
            <th>Supplier Company Name</th>
            <th>Supplier Company Code</th>
            <th className="boolean">CTQ</th>
            <th>QPL Last Updated By</th>
            <th>QPL Last Updated Date</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props.items.map((item, key) =>
              <Element key={item.partNumber} item={item} />
            )
          }
        </tbody>
      </table>
    )
  }
}

export default Table;