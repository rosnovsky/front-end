import React, { Component } from 'react';
import './App.css';

class Element extends Component {
  formatDate(date) {
    const inputDate = new Date(date);
    return `${inputDate.getMonth() + 1}/${inputDate.getDate()}/${inputDate.getFullYear()}`
  }

  render() {
    return (
      <tr>
        <th>
          {this.props.item.partNumber}
        </th>
        <th>
          {this.props.item.partRevision}
        </th>
        <th>
          {this.props.item.partName}
        </th>
        <th>
          {this.props.item.toolDieSetNumber}
        </th>
        <th className="boolean">
          {this.props.item.isQualified ? "✅" : ""}
        </th>
        <th className="boolean">
          {this.props.item.openPo ? "✅" : ""}
        </th>
        <th>
          {this.props.item.jurisdiction}
        </th>
        <th>
          {this.props.item.classification}
        </th>
        <th>
          {this.props.item.supplierName}
        </th>
        <th>
          {this.props.item.supplierCode}
        </th>
        <th className="boolean">
          {this.props.item.ctq ? "✅" : ""}
        </th>
        <th>
          {this.props.item.lastUpdatedBy}
        </th>
        <th>
          {this.formatDate(this.props.item.lastUpdatedDate)}
        </th>
      </tr>
    )
  }
}

export default Element;