[![Netlify Status](https://api.netlify.com/api/v1/badges/228ca98c-4dcf-4ce5-8ec7-f33a4d81aa1c/deploy-status)](https://app.netlify.com/sites/qpl-list/deploys)

# Welcome to Net-Inspect

This submission is setup with React.

1. Run `yarn` (or `npm install`)
2. Run `yarn start` (or `npm start`/`npm run start`)

The project is now available at `http://localhost:3000/`